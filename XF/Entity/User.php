<?php


namespace AnzahTools\CustomFieldsPermission\XF\Entity;


/**
 * Class User
 * @package AnzahTools\CustomFieldsPermission\XF\Entity
 */
class User extends XFCP_User
{
    /**
     * @return bool
     */
    public function canSeePostMessage ()
    {
        return $this->hasPermission('forum', 'at_ctfvp_post_message');
    }
    /**
     * @return bool
     */
    public function canSeeMacroFieldsAfter ()
    {
        return $this->hasPermission('forum', 'at_ctfvp_fields_after');
    }
    /**
     * @return bool
     */
    public function canSeeMacroFieldsBefore ()
    {
        return $this->hasPermission('forum', 'at_ctfvp_fields_before');
    }
    /**
     * @return bool
     */
    public function canSeeMacroFieldsStatusBlock ()
    {
        return $this->hasPermission('forum', 'at_ctfvp_statusBlock');
    }

}